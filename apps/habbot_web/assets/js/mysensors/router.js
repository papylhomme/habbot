import MySensorsRoot from './root'

import MySensorsActivity from './activity_main'
import Networks from 'mysensors/networks/networks'
import Nodes from 'mysensors/nodes/nodes'
import Sensors from 'mysensors/sensors/sensors'
import Events from './events'

import EditNetworkDialog from 'mysensors/networks/edit_network_dialog'

import NetworkDialog from 'mysensors/networks/network_dialog'
import NetworkInformationPanel from 'mysensors/networks/network_information_panel'
import NetworkTransportPanel from 'mysensors/networks/network_transport_panel'

import CommandsPanel from 'mysensors/commands/commands_panel'
import NodeDialog from 'mysensors/nodes/node_dialog'
import NodeInformationPanel from 'mysensors/nodes/node_information_panel'

import SensorDialog from 'mysensors/sensors/sensor_dialog'
import SensorInformationPanel from 'mysensors/sensors/sensor_information_panel'



export default {
  path: '/mysensors', component: MySensorsRoot,

  //TODO move children in dynamic router included by MySensorsRoot when vue-router support removing routes (v3)
  children: [
    { path: 'networks/new', component: EditNetworkDialog },
    { path: 'network/:network/edit', component: EditNetworkDialog, props: (route) => ({network: route.params.network}) },
    { path: 'network/:network', component: NetworkDialog, props: (route) => ({network: route.params.network}),
      children: [
        { path: '/', redirect: 'information' },
        { path: 'information', component: NetworkInformationPanel, props: (route) => ({network: route.params.network}) },
        { path: 'transport', component: NetworkTransportPanel, props: (route) => ({network: route.params.network}) },
      ]
    },
    { path: 'node/:node', component: NodeDialog, props: (route) => ({node: route.params.node}),
      children: [
        { path: '/', redirect: 'information' },
        { path: 'information', component: NodeInformationPanel, props: (route) => ({node: route.params.node}) },
        { path: 'commands', component: CommandsPanel, props: (route) => ({node: route.params.node, hideNode: true}) },
      ]
    },
    { path: 'sensor/:sensor', component: SensorDialog, props: (route) => ({sensor: route.params.sensor}),
      children: [
        { path: '/', redirect: 'information' },
        { path: 'information', component: SensorInformationPanel, props: (route) => ({sensor: route.params.sensor}) },
        { path: 'commands', component: CommandsPanel, props: (route) => ({sensor: route.params.sensor, hideNode: true, hideSensor: true}) },
      ]
    },
    { path: '/', component: MySensorsActivity,
      children: [
        { path: '/', redirect: 'nodes' },
        { path: 'networks', component: Networks },
        { path: 'nodes', component: Nodes, props: (route) => ({network: route.query.network}) },
        { path: 'sensors', component: Sensors, props: (route) => ({node: route.query.node}) },
        { path: 'events', component: Events },
      ]
    }
  ]
}

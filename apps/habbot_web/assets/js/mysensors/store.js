import Vue from "vue"


// Helper for async call and loader support
function fetch(loader, channel, call, mutation, {commit, dispatch}) {
  dispatch('wait/start', loader, { root: true });

  //TODO handle errors
  channel.push(call)
    .receive("ok", res => {
      commit(mutation, res)
      dispatch('wait/end', loader, { root: true });
    })
}

export default {
  namespaced: true,
  state: {
    commands: [
      {id: 0, name: 'Presentation'},
      {id: 1, name: 'Set'},
      {id: 2, name: 'Req'},
      {id: 3, name: 'Internal'},
      {id: 4, name: 'Stream'}
    ],
    types: {},
    networks: {},
    nodes: {},
    sensors: {}
  },
  mutations: {
    SET_TYPES(state, types) {
      state.types = types
    },

    SET_NETWORKS(state, networks) {
      state.networks = networks
    },

    SET_NODES(state, nodes) {
      state.nodes = nodes
    },

    SET_SENSORS(state, sensors) {
      state.sensors = sensors
    },

    ADD_NETWORK(state, {uuid, ...remaining}) {
      if(state.networks[uuid] != null) return
      Vue.set(state.networks, uuid, remaining)
    },

    DELETE_NETWORK(state, {network}) {
      Vue.delete(state.networks, network)
    },

    UPDATE_NETWORK_STATUS(state, {network, status, ...remaining}) {
      if(state.networks[network] == null) return
      Vue.set(state.networks[network], 'status', status)

      switch(status) {
        case 'stopped':
          Vue.delete(state.networks[network], 'info')
          Vue.delete(state.networks[network], 'error')
          break;

        case 'running':
          Vue.delete(state.networks[network], 'error')
          Vue.set(state.networks[network], 'info', remaining['info'])
          break;

        case 'error':
          Vue.delete(state.networks[network], 'info')
          Vue.set(state.networks[network], 'error', remaining['error'])
          break;
      }
    },

    ADD_NODE(state, node) {
      if(state.nodes[node.uuid] != null) return
      Vue.set(state.nodes, node.uuid, node)
    },

    UPDATE_NODE(state, node) {
      if(state.nodes[node.uuid] == null) return
      Vue.set(state.nodes, node.uuid, Object.assign({}, state.nodes[node.uuid], node))
    },

    UPDATE_SENSOR_DATA(state, {sensor, type, data}) {
      if(state.sensors[sensor] == null) return
      Vue.set(state.sensors[sensor].data, type, data)
    },
  },
  actions: {
    // Fetch MySensors types
    fetchTypes(context, channel) {
      fetch('ms:fetch-types', channel, "variable_types", 'SET_TYPES', context)
    },

    // Fetch MySensors networks
    fetchNetworks(context, channel) {
      fetch('ms:fetch-networks', channel, "networks", 'SET_NETWORKS', context)
    },

    // Fetch MySensors nodes
    fetchNodes(context, channel) {
      fetch('ms:fetch-nodes', channel, "nodes", 'SET_NODES', context)
    },

    // Fetch MySensors sensors
    fetchSensors(context, channel) {
      fetch('ms:fetch-sensors', channel, "sensors", 'SET_SENSORS', context)
    },

    // Update a network with the given event
    updateNetwork({commit}, event) {
      commit('UPDATE_NETWORK_STATUS', event)
    },

    // Add a network with the given event
    addNetwork({commit}, event) {
      commit('ADD_NETWORK', event)
    },

    // Delete a network with the given event
    deleteNetwork({commit}, event) {
      commit('DELETE_NETWORK', event)
    },

    // Update a node with the given event
    addNode({commit}, event) {
      commit('ADD_NODE', event)
    },

    // Update a node with the given event
    updateNode({commit}, event) {
      commit('UPDATE_NODE', event)
    },

    // Update a sensor's data with the given event
    updateSensorData({commit}, event) {
      commit('UPDATE_SENSOR_DATA', {
        sensor: event.sensor,
        type: event.type,
        data: event.new
      })
    },
  }
}

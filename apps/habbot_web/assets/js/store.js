import Vue from 'vue'
import Vuex from 'vuex'
import VuexNow from 'vuex-now/dist/vuex-now'

Vue.use(Vuex)

/* VuexNow(interval) - inverval: time in miliseconds for autoupdating the now variable */
const now = VuexNow(10 * 1000)

// Helper for async call and loader support
function fetch(loader, channel, call, mutation, {commit, dispatch}) {
  dispatch('wait/start', loader, { root: true });

  //TODO handle errors
  channel.push(call)
    .receive("ok", res => {
      commit(mutation, res)
      dispatch('wait/end', loader, { root: true });
    })
}


export default new Vuex.Store({
  plugins: [now],
  state: {
    initialized: {
      values: false,
      places: false,
    },
    values: {},
    places: {},
  },
  mutations: {
    SET_VALUES(state, values) {
      state.values = values
      state.initialized.values = true
    },

    UPDATE_VALUE(state, {uuid, value, timestamp}) {
      if(state.values[uuid] == null) return
      Vue.set(state.values[uuid], 'value', value)
      Vue.set(state.values[uuid], 'timestamp', timestamp)
    },

    FLAG_VALUE(state, {uuid, outdated}) {
      if(state.values[uuid] == null) return
      Vue.set(state.values[uuid], 'outdated', outdated)
    },


    SET_PLACES(state, places) {
      state.places = places
      state.initialized.places = true
    },

    ADD_PLACE(state, place) {
      Vue.set(state.places, place.uuid, place)
    },

    UPDATE_PLACE(state, place) {
      Vue.set(state.places, place.uuid, place)
    },

    REMOVE_PLACE(state, {uuid}) {
      Vue.delete(state.places, uuid)
    },
  },
  actions: {
    // Fetch Habbot Values
    fetchValues(context, channel) {
      fetch('habbot:fetch-values', channel, "values", 'SET_VALUES', context)
    },

    // Update a value with the given event
    updateValue({commit}, event) {
      commit('UPDATE_VALUE', event)
    },

    // Flag a value
    flagValue({commit}, params) {
      commit('FLAG_VALUE', params)
    },

    // Fetch Habbot Places
    fetchPlaces(context, channel) {
      fetch('habbot:fetch-places', channel, "places", 'SET_PLACES', context)
    },

    // Add a new place
    addPlace({commit}, event) {
      commit('ADD_PLACE', event)
    },

    // Update a place
    updatePlace({commit}, event) {
      commit('UPDATE_PLACE', event)
    },

    // Remove a place
    removePlace({commit}, event) {
      commit('REMOVE_PLACE', event)
    },
  }
})

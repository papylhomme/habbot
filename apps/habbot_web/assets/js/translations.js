export default {
  en: {
    uuid: "UUID",
    details: "Details",
    all: "All",
    search: "Search",
    never: "never",
    keep: "Keep",
    information: "Information",
    status: "Status",
    name: "Name",
    description: "Description",
    actionYes: "Yes",
    actionNo: "No",
    actionOK: "Ok",
    actionCancel: "Cancel",
    actionCreate: "Create",
    networkError: "Network error: {error}",

    sections: {
      home: "Home",
      places: "Places",
      values: "Values",
      schedules: "Schedules",
      alerts: "Alerts",
      system: "System",
      settings: "Settings",
      mysensors: "MySensors",
    },

    command: {
      error: "Command error",
      timeout: "Command timeout !",
    }
  },
  fr: {
    uuid: "UUID",
    details: "Détails",
    all: "Tous",
    search: "Recherche",
    never: "jamais",
    keep: "Garder",
    information: "Informations",
    status: "Status",
    name: "Nom",
    description: "Description",
    actionYes: "Oui",
    actionNo: "Non",
    actionOK: "Ok",
    actionCancel: "Annuler",
    actionCreate: "Créer",
    networkError: "Erreur réseau : {error}",

    sections: {
      home: "Accueil",
      places: "Lieux",
      values: "Valeurs",
      schedules: "Planning",
      alerts: "Alertes",
      system: "Système",
      settings: "Paramètres",
      mysensors: "MySensors",
    },

    command: {
      error: "Erreur lors de l'éxécution de la commande !",
      timeout: "Timeout lors de l'éxécution de la commande !",
    }
  }
}

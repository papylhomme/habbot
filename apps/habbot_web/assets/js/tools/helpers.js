import Vue from 'vue'
import moment from 'moment'

import store from '../store.js'


export default { init: function() {
  // fromNow filter to format timestamp
  Vue.filter('fromNow', value => value ? moment(value).from(store.state.now.now) : '')


  /**
   * Registered UI components
   */
  Vue.component('layout', require("tools/activities/layout").default) // The application main layout
  Vue.component('panel-transition', require("tools/ui/panel_transition").default) // A simple content section
  Vue.component('value', require("values/value").default) // A simple content section
  Vue.component('my-snackbar', require("tools/ui/my_snackbar").default) // A snackbar
  Vue.component('my-icon-button', require("tools/ui/my_icon_button").default) // An icon button
}}

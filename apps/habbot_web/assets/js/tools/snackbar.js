// Include logic and methods to work with a snackbar
export function withSnackbar(config) {
  config = config || {}

  return {
    data() { return {
      snackbar: {
        active: false,
        message: '',
        duration: config.duration || 2000,
        position: config.position || 'center'
      },
    }},
    methods: {
      // Display a message using the snackbar
      showSnackbar: function(message) {
        this.snackbar.message = message
        this.snackbar.active = true
      },
    }
  }
}


// Include logic and methods to work with a snackbar, and provide support
// to the children hierarchy through 'provide'
export function provideSnackbar(config) {
  return {
    ...withSnackbar(config),
    provide: function() { return { 'snackbar': this.snackbar } },
  }
}


// Include methods to work with a snackbar provided by an ancestor
export const usingSnackbar = {
  inject: ['snackbar'],

  methods: {
    // Display a message using the snackbar
    showSnackbar: function(message) {
      this.snackbar.message = message
      this.snackbar.active = true
    },
  }
}

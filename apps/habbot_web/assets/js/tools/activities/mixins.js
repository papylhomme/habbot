import Vue from 'vue'

import { provideSnackbar } from 'tools/snackbar'


/*
 * Base mixin
 */
const BaseMixin = {
  mixins: [ provideSnackbar() ],

  methods: {
    closeActivity() { this.$router.go(-1) }
  }
}


/*
 * Simple activity mixin
 */
export const Activity = {
  ...BaseMixin,
  components: { 'activity': require("tools/activities/layout").default },
}

/*
 * Simple dialog mixin
 */
export const DialogActivity = {
  ...BaseMixin,
  components: { 'dialog-activity': require("tools/activities/dialog").default },
}

/*
 * Bottom bar activity mixin
 */
export const BottomBarActivity = {
  ...BaseMixin,
  components: { 'bottom-bar-activity': require("tools/activities/bottom_bar").default },
}



/**
 * Navigation activity mixin
 */
export const NavigationActivity = {
  ...BaseMixin,
  components: {
    'navigation-activity': require("tools/activities/navigation").default,
    'navigation-item': require("tools/activities/navigation_item").default,
  },
}

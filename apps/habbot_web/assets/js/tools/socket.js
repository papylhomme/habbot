import {Socket} from "phoenix"

class WebSocket {

  constructor() {
    this.socket = null
  }


  isConnected() {
    return this.socket != null
  }


  connect(params, connectedHandler, errorHandler) {
    if(this.socket != null)
      this.disconnect()
    else {
      this.socket = new Socket("/socket", {params: params})
      this.socket.onError(errorHandler)
    }

    this.socket.connect()
    connectedHandler()
  }


  disconnect() {
    if(this.socket != null)
      this.socket.disconnect()
  }


  channel(name, params) {
    if(this.socket == null)
      return null

    return this.socket.channel(name, params)
  }

}

export default new WebSocket()

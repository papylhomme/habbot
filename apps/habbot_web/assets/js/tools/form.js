/**
 * Export helper Mixin as default
 */
export default {
  data: function() { return {
    form: {}
  }},
  methods: {
    data() {
      return this.form
    },
    validate() {
      return this.$refs.form.validate()
    },
  }
}

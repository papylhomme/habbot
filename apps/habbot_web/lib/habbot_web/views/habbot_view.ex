defmodule HabbotWeb.HabbotView do
  use HabbotWeb, :view

  def render("time.json", _) do
    %{time: DateTime.utc_now |> DateTime.to_unix(:millisecond)}
  end


  def render("values.json", _) do
    for v <- Habbot.Values.list, into: %{} do
      {v, Habbot.Values.get(v)}
    end
  end


  def render("places.json", _) do
    Habbot.Places.list
  end

  def render("value_event.json", %{type: _type, payload: payload}) do
    payload
  end

  def render("place_event.json", %{type: _type, payload: payload}) do
    payload
  end

end

require Protocol
Protocol.derive(Jason.Encoder, Habbot.Value.Data)
Protocol.derive(Jason.Encoder, Habbot.Place)

defmodule HabbotWeb.Router do
  use HabbotWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :auth do
    plug Guardian.Plug.Pipeline,
      error_handler: HabbotWeb.AuthErrorHandler,
      module: HabbotWeb.Guardian
    plug Guardian.Plug.VerifyHeader
    plug Guardian.Plug.VerifySession
    plug Guardian.Plug.LoadResource
  end

  scope "/", HabbotWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  post "/api/auth/login", HabbotWeb.API.AuthController, :login

  # Other scopes may use custom stacks.
  scope "/api", HabbotWeb.API do
    pipe_through [:api, :auth]

    scope "/auth" do
      post "/logout", AuthController, :logout
      post "/info", AuthController, :info
    end

    get "/status", StatusController, :index
  end
end

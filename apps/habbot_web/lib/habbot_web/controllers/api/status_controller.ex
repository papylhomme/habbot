defmodule HabbotWeb.API.StatusController do
  use HabbotWeb, :controller

  def index(conn, _params) do
    json conn, %{time: DateTime.utc_now |> DateTime.to_unix(:milliseconds)}
  end

end


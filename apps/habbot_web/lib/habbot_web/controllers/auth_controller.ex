defmodule HabbotWeb.API.AuthController do
  use HabbotWeb, :controller

  # Simple application username
  @username Application.get_env(:habbot_web, :username, "admin")

  # Simple application password
  @password Application.get_env(:habbot_web, :password, "admin")


  def login(conn, params) do
    if Map.get(params, "username") == @username and Map.get(params, "password") == @password do
      user = %{id: "1"}

      conn =
        conn
        |> HabbotWeb.Guardian.Plug.sign_in(user, %{}, ttl: {1, :day})
        |> put_resp_header("content-type", "application/json; charset=utf-8")

      # don't chain this as `current_token(conn)` would read the original unconnected conn
      conn
      |> send_resp(200, Jason.encode!(%{token: HabbotWeb.Guardian.Plug.current_token(conn)}))
    else
      conn
      |> send_resp(401, "Invalid credentials")
    end
  end


  def logout(conn, _params) do
    conn
    |> HabbotWeb.Guardian.Plug.sign_out
    |> send_resp(200, "")
  end


  def info(conn, _params) do
    i = HabbotWeb.Guardian.Plug.current_resource(conn)
    send_resp(conn, 200, Jason.encode!(i))
  end

end



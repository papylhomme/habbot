defmodule HabbotWeb.MySensorsCommandsChannel do
  use Phoenix.Channel
  require Logger

  alias MySensors.Types
  alias MySensors.Node
  alias MySensors.Sensor
  alias MySensors.MessageQueue
  alias HabbotWeb.MySensorsView


  # OnJoin: setup the channel for a node
  def join("mysensors#commands:node_" <> node_uuid, _params, socket) do
    Node.subscribe_nodes_commands(node_uuid)

    socket = assign(socket, :node_uuid, node_uuid)
    {:ok, _list_commands(socket), socket}
  end


  # OnJoin: setup the channel for a sensor
  def join("mysensors#commands:sensor_" <> sensor_uuid, _params, socket) do
    info =
      sensor_uuid
      |> MySensors.by_uuid
      |> Sensor.info

    node_uuid = Map.fetch!(info, :node)

    Node.subscribe_nodes_commands(node_uuid)

    socket =
      socket
      |> assign(:node_uuid, node_uuid)
      |> assign(:sensor_uuid, sensor_uuid)
      |> assign(:sensor_id, Map.fetch!(info, :sensor_id))

    {:ok, _list_commands(socket), socket}
  end


  # Message: List the pending commands for the current queue
  def handle_in("pending_commands", _params, socket) do
    {:reply, {:ok, _list_commands(socket)}, socket}
  end


  # Message: Send a new command
  def handle_in("command", message, socket) do
    case socket.assigns do
      %{sensor_uuid: sensor_uuid} ->
        sensor_uuid
        |> MySensors.by_uuid
        |> Sensor.command(Types.command_type(message["command"]), Types.variable_type(message["type"]), message["payload"])

      %{node_uuid: node_uuid} ->
        node_uuid
        |> MySensors.by_uuid
        |> Node.command(message["sensor_id"], Types.command_type(message["command"]), Types.variable_type(message["type"]), message["payload"])
    end

    {:reply, {:ok, %{status: :sent}}, socket}
  end


  # Message: Flush the queue
  def handle_in("flush", _message, socket) do
    filter =
      case socket.assigns do
        %{sensor_id: sensor_id} ->
          (fn cmd -> cmd.message.child_sensor_id == sensor_id end)
        _ -> (fn _ -> true end)
      end

    socket
    |> _queue
    |> MessageQueue.flush(filter)

    {:reply, :ok, socket}
  end


  # Message: Clear the pending commands
  def handle_in("clear", _message, socket = %{assigns: %{sensor_id: sensor_id}}) do
    socket
    |> _queue
    |> MessageQueue.remove(fn cmd -> cmd.message.child_sensor_id == sensor_id end)

    {:reply, :ok, socket}
  end

  def handle_in("clear", _message, socket) do
    socket
    |> _queue
    |> MessageQueue.clear

    {:reply, :ok, socket}
  end


  # Message: Remove a command
  def handle_in("remove", id, socket) do
    command_id = String.to_integer(id)

    socket
    |> _queue
    |> MessageQueue.remove(fn cmd -> cmd.id == command_id end)

    {:reply, :ok, socket}
  end


  # Message: Resent a command
  def handle_in("resend", id, socket) do
    command_id = String.to_integer(id)

    socket
    |> _queue
    |> MessageQueue.flush(fn cmd -> cmd.id == command_id end)

    {:reply, :ok, socket}
  end


  # Message: Fallback for unrecognized messages
  def handle_in(msg, payload, socket) do
    Logger.warn "#{__MODULE__} received an unexpected message from socket: #{msg}\n#{inspect payload}"
    {:noreply, socket}
  end


  # Event: Handle cleared event from queue
  def handle_info({:mysensors, :nodes_commands, {:cleared}}, socket) do
    push(socket, "node_commands_cleared", %{})
    {:noreply, socket}
  end


  # Event: Handle command events from queue
  def handle_info({:mysensors, :nodes_commands, event = {_status, msg}}, socket) do
    unless Map.has_key?(socket.assigns, :sensor_id) and socket.assigns.sensor_id != msg.message.child_sensor_id do
      push(socket, "node_commands", MySensorsView.render("node_command.json", event: event))
    end

    {:noreply, socket}
  end


  # Event: Fallback for unrecognized messages
  def handle_info(msg, socket) do
    Logger.warn "#{__MODULE__} received an unexpected message from process:\n#{inspect msg}"
    {:noreply, socket}
  end


  # List the pending commands associated with the channel
  defp _list_commands(socket) do
    case socket.assigns do
      %{sensor_uuid: sensor_uuid} ->
        MySensorsView.render("pending_sensor_commands.json", %{sensor_uuid: sensor_uuid})
      %{node_uuid: node_uuid} ->
        MySensorsView.render("pending_node_commands.json", %{node_uuid: node_uuid})
    end
  end


  # Get the MessageQueue pid from the socket's assigns
  defp _queue(socket) do
    socket.assigns.node_uuid
    |> MySensors.by_uuid
    |> Node.queue
  end

end


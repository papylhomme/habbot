defmodule Habbot.PubSub do

  @moduledoc """
  A `Phoenix.PubSub` server for Habbot
  """

  @doc """
  Defines the specification for a supervisor
  """
  def child_spec(_args) do
    Supervisor.child_spec(
      %{start: {Phoenix.PubSub.PG2, :start_link, [__MODULE__, []]}, id: __MODULE__},
      []
    )
  end


  @doc """
  Broadcast a `message` to the `topic`
  """
  def broadcast(topic, message) do
    Phoenix.PubSub.broadcast(__MODULE__, topic, message)
  end


  @doc """
  Subscribe to the `topic`'s messages
  """
  def subscribe(topic) do
    Phoenix.PubSub.subscribe(__MODULE__, topic)
  end


  @doc """
  Unsubscribe from the `topic`'s messages
  """
  def unsubscribe(topic) do
    Phoenix.PubSub.unsubscribe(__MODULE__, topic)
  end

end

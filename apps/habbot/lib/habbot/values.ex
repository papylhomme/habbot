defmodule Habbot.Values do

  @moduledoc """
  Repository for values
  """
  use GenServer, start: {__MODULE__, :start_link, []}

  require Logger

  # Topic's constants
  def registration_topic, do: "values"
  def events_topic, do: "values_events"
  def events_topic(value), do: "value_#{value}_events"


  @doc """
  Start the server
  """
  def start_link() do
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)
  end


  @doc """
  Register a new value using `module` and the given params
  """
  def register(module, value, params) do
    GenServer.call(__MODULE__, {:register, module, value, params})
  end


  @doc """
  Unregister the value
  """
  def unregister(value) do
    GenServer.call(__MODULE__, {:unregister, value})
  end


  @doc """
  List the values
  """
  def list do
    GenServer.call(__MODULE__, :list)
  end


  @doc """
  Get a value
  """
  def get(value) do
    GenServer.call(__MODULE__, {:get, value})
  end


  @doc """
  Watch values events
  """
  def watch() do
    Habbot.PubSub.subscribe(events_topic())
  end


  @doc """
  Watch for `value` events
  """
  def watch(value) do
    Habbot.PubSub.subscribe(events_topic(value))
  end


  @doc """
  Unwatch values events
  """
  def unwatch() do
    Habbot.PubSub.unsubscribe(events_topic())
  end


  @doc """
  Unwatch `value` events
  """
  def unwatch(value) do
    Habbot.PubSub.unsubscribe(events_topic(value))
  end


  # Init the server
  def init(nil) do
    # Trap exits for values processes
    Process.flag(:trap_exit, true)

    {:ok, %{by_uuids: %{}, by_pids: %{}}}
  end


  # Handle register call
  def handle_call({:register, module, value, params}, _from, state) do
    {:ok, pid} = apply(module, :start_link, [value, params])
    new_state =
      state
      |> put_in([:by_pids, pid], value.uuid)
      |> put_in([:by_uuids, value.uuid], pid)

    Habbot.PubSub.broadcast(registration_topic(), {:habbot, :value_registered, value})

    {:reply, :ok, new_state}
  end


  # Handle unregister call
  def handle_call({:unregister, value}, _from, state) do
    case Map.get(state.by_uuids, value) do
      pid when is_pid(pid) ->
        Habbot.PubSub.broadcast(registration_topic(), {:habbot, :value_unregistered, %{uuid: value}})
        Process.exit(pid, :kill)
        {:reply, :ok, state}
      _ -> {:reply, :ok, state}
    end
  end


  # Handle list call
  def handle_call(:list, _from, state) do
    {:reply, Map.keys(state.by_uuids), state}
  end


  # Handle get call
  def handle_call({:get, value}, _from, state) do
    res =
      state.by_uuids
      |> Map.get(value)
      |> Habbot.Value.get

    {:reply, res, state}
  end


  # Handle value process exiting
  def handle_info({:EXIT, from, _reason}, state) do
    # TODO restart the value or maybe delegate this to the originating controller ?
    case pop_in(state, [:by_pids, from]) do
      {nil, _pids} -> {:noreply, state}
      {uuid, pids} -> {:noreply, %{state | by_pids: pids, by_uuids: Map.delete(state.by_uuids, uuid)}}
    end
  end

end

defmodule Habbot.MySensorsValue do

  @moduledoc """
  An `Habbot.Value` mapped from a MySensors sensor value
  """
  use Habbot.Value

  alias MySensors.Sensor
  require Logger


  @doc """
  Register a new value from the `sensor` for the given `data_type`
  """
  def register(sensor, data_type, source) do
    uuid = UUID.uuid5(sensor.uuid, data_type |> Atom.to_string)
    type =
      case data_type do
        V_HUM -> :humidity
        V_TEMP -> :temperature
        V_PRESSURE -> :pressure
        _ -> :generic
      end

    value = %Value.Data{uuid: uuid, type: type, source: source}
    value =
      case Map.get(sensor.data, data_type) do
        {v, t} -> %{value | value: v, timestamp: t}
        _ -> value
      end

    Habbot.Values.register(
      __MODULE__,
      value,
      %{
        mysensors_type: data_type,
        sensor: sensor.uuid
      }
    )
  end


  @doc false
  # Init callback from Habbot.Value, register to sensor's events
  def on_init(_value, params) do
    Sensor.subscribe_sensors_events(params.sensor)
    %{mysensors_type: params.mysensors_type}
  end


  # Handle events from the related sensor
  def on_info({:mysensors, :sensors_events, e = %Sensor.ValueUpdatedEvent{}}, state = %{mysensors_type: mysensors_type}) do
    case e do
      %{type: type, new: {value, time}} ->
        case type do
          ^mysensors_type -> Value.update(state, value, time)
          _ -> state
        end
      _ ->
        Logger.warn("Unrecognized value updated event format")
        state
    end
  end

end

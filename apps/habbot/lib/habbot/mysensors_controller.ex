defmodule Habbot.MySensorsController do

  @moduledoc """
  MySensors controller
  """
  use GenServer, start: {__MODULE__, :start_link, []}

  require Logger


  @doc "Start the server"
  @spec start_link() :: GenServer.on_start
  def start_link do
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)
  end


  # Initialize the controller
  def init(nil) do
    alias Habbot.MySensorsValue

    networks = MySensors.networks
    nodes = MySensors.nodes
    sensors = MySensors.sensors

    # Loop over sensors and register mapped values
    for {_uuid, sensor} <- sensors do
      node = nodes[sensor.node]
      network = networks[node.network]
      source = "MySensors / #{network.config.name} / #{_node_desc(node)} / #{_sensor_desc(sensor)}"

      case sensor.type do
        S_HUM -> MySensorsValue.register(sensor, V_HUM, source)
        S_TEMP -> MySensorsValue.register(sensor, V_TEMP, source)
        S_BARO -> MySensorsValue.register(sensor, V_PRESSURE, source)
        S_HEATER -> MySensorsValue.register(sensor, V_TEMP, source)
        _ -> Logger.warn "Unknown MySensors sensor type: #{sensor.type}" # Do nothing
      end
    end

    {:ok, %{}}
  end

  defp _node_desc(node) do
    "#{node.sketch_name}##{node.node_id}"
  end


  defp _sensor_desc(sensor) do
    desc =
      case sensor do
        %{desc: desc} when is_nil(desc) or desc == "" -> Atom.to_string(sensor.type) |> String.replace_leading("Elixir.", "")
        %{desc: desc} -> desc
      end

    "#{desc}##{sensor.sensor_id}"
  end

end

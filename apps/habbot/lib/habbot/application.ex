defmodule Habbot.Application do
  @moduledoc """
  The Habbot Application Service.

  The habbot system business domain lives in this application.

  Exposes API to clients such as the `HabbotWeb` application
  for use in channels, controllers, and elsewhere.
  """
  use Application


  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    Supervisor.start_link([
      Habbot.PubSub,
      Habbot.Values,
      Habbot.Places,
      Habbot.MySensorsController
    ], strategy: :one_for_one, name: Habbot.Supervisor)
  end
end

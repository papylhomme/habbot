defmodule Habbot.Value do

  @moduledoc """
  A value
  """

  @doc "Callback used to initialize the value server"
  @callback on_init(any(), term()) :: map()

  @doc "Callback used to handle info message"
  @callback on_info(any(), any()) :: any()


  @doc "Get the value from a value `server`"
  def get(server) do
    GenServer.call(server, :get)
  end


  defmodule Data do
    defstruct uuid: nil,
      type: "generic",
      source: nil,
      outdated: true,
      value: nil,
      timestamp: nil
  end





  defmodule Stats do
    @moduledoc "Statistics for habbot value"
    defstruct ttl: nil, lastUpdate: nil, lastDurations: []

    def update(stats, _value, now \\ DateTime.utc_now) do
      case stats do
        %{lastUpdate: nil} -> %{stats | lastUpdate: now}
        %{lastUpdate: lastUpdate} ->
          lastDurations = Enum.slice([DateTime.diff(now, lastUpdate) | stats.lastDurations], 0, 10)

          {sum, count} = Enum.reduce(lastDurations, {0, 0}, fn duration, {sum, count} -> {sum + duration, count + 1} end)
          mean = sum / count
          deviation = :math.sqrt(Enum.reduce(lastDurations, 0, fn duration, sum -> sum + :math.pow(duration - mean, 2) end))

          if count > 5 and deviation < mean / 2 do
            %{stats | ttl: mean * 1.5, lastUpdate: now, lastDurations: lastDurations}
          else
            %{stats | lastUpdate: now, lastDurations: lastDurations}
          end
      end
    end
  end



  @doc "Generate the stubs for a value server"
  defmacro __using__(_) do
    quote do
      use GenServer, start: {__MODULE__, :start_link, [:data, :params]}

      alias Habbot.Value
      alias Habbot.Value.Stats

      @behaviour Habbot.Value


      @doc "Start a value server with the given `params`"
      def start_link(data, params) do
        GenServer.start_link(__MODULE__, {data, params})
      end


      # Initialize the Value server
      def init({data, params}) do
        state = %{data: data, stats: %Stats{}}
        {:ok, Map.merge(state, on_init(data, params)), Value.timeout(state)}
      end


      # Handle get call
      def handle_call(:get, _from, state = %{data: data}) do
        {:reply, data, state, Value.timeout(state)}
      end


      def handle_info(:timeout, state = %{data: data}) do
        Value.event(data.uuid, {:habbot, :value_outdated, %{uuid: data.uuid}})
        {:noreply, %{state | data: %{data | outdated: true}}}
      end


      def handle_info(message, state) do
        state = on_info(message, state)
        {:noreply, state, Value.timeout(state)}
      end
    end
  end


  def event(value, event) do
    Habbot.PubSub.broadcast(Habbot.Values.events_topic(), event)
    Habbot.PubSub.broadcast(Habbot.Values.events_topic(value), event)
  end


  def timeout(%{data: %{outdated: true}}), do: :infinity
  def timeout(%{stats: %Stats{lastUpdate: lastUpdate, ttl: ttl}}) when is_nil(lastUpdate) or is_nil(ttl), do: :infinity
  def timeout(%{stats: %Stats{lastUpdate: lastUpdate, ttl: ttl}}) do
    case DateTime.diff(DateTime.utc_now, lastUpdate) do
      x when x > ttl -> 1
      x -> trunc(ttl - x) * 1000
    end
  end

  def update(state = %{data: data}, new_value, timestamp \\ DateTime.utc_now) do
    stats = Stats.update(state.stats, new_value, timestamp)
    if(data.outdated, do: event(data.uuid, {:habbot, :value_uptodate, %{uuid: data.uuid}}))

    data = %Data{data | outdated: false, value: new_value, timestamp: stats.lastUpdate}
    event(data.uuid, {:habbot, :value_event, Map.take(data, [:uuid, :value, :timestamp])})

    %{state | data: data, stats: stats}
  end

end
